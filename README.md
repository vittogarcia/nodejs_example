**NodeJS Example with MongoDB local connection**

This is a basic example of API CRUD 

---

## Installation

1. Install **npm** on your computer
2. Install **mongoDB** and **Robo3T Mongo GUI**
3. You can use **Postman** or other API Client
4. Create a DB from Robo3T and configure **index.js** 
5. Use **routes/articles.js** to check API Routes
6. Thanks