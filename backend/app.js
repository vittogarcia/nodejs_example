'use strict'

// Cargar Modulos de NodeJS para crear el servidor
var express = require('express')
var bodyParser = require('body-parser')

// Ejecutar Express
var app = express()

// Cargar ficheros de rutas
var article_routes = require('./routes/article')

// Middlewares para usar el body parser
// convertir cualquier peticion a JSON
app.use(bodyParser.urlencoded({extended:false}))
app.use(bodyParser.json())

// CORS es el acceso cruzado entre dominio y permitir peticiones desde cualquier frontend (angular, vue, react, etc)
// Configurar cabeceras y cors
app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method');
    res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
    res.header('Allow', 'GET, POST, OPTIONS, PUT, DELETE');
    next();
});

// Añadir prefijos de rutas / cargar rutas
app.use('/api', article_routes)

// Exportar Modulo
module.exports = app