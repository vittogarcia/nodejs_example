'use strict'

// Dependencias
var validator = require('validator')
var fs = require('fs')
var path = require('path')
// Modelos
var Article = require('../models/article')

var controller = {
    //Metodos del controlador
    datosCurso: (req, res) => {
        var prueba = req.body.hola

        return res.status(200).send({
            curso: 'Probando el NodeJS/MongoDB',
            autor: 'VittoGarcia',
            url: 'www.puntowebmx.net'
        })
    },

    test: (req, res) => {
        return res.status(200).send({
            message: 'Prueba de controlador de articulos'
        })
    },

    save: (req, res) => {
        var params = req.body

        // Validacion de datos
        try {
            // validar los parametros con la dependencia validator
            var validate_title = !validator.isEmpty(params.title)
            var validate_content = !validator.isEmpty(params.content)
        }catch(error) {
            return res.status(200).send({
                status: 'error',
                message: 'Faltan datos por enviar !!!'
            })
        }

        if(validate_title && validate_content){ 
            // Crear objeto para guardar
            var article = new Article()

            // Asignar los valores
            article.title = params.title
            article.content = params.content
            article.image = null

            // Guardar el articulo
            // Utiliza funcion callback
            article.save((err, articleStored) => {
                if(err || !articleStored) {
                    return res.status(404).send({
                        status: 'error',
                        message: 'Los datos no son válidos !!!'
                    })
                }
                // Devolver respuesta
                return res.status(200).send({
                    status: 'success',
                    article: articleStored
                })                
            })
        }
        else {
            return res.status(200).send({
                status: 'error',
                message: 'Faltan datos por validar'
            })
        }
    },

    getArticles: (req, res) => {
        // Metodos para ordenar .sort(id, -id)
        var query = Article.find({})
        var last = req.params.last;
        if(last || last != undefined) {
            query.limit(5);
        }
        // Busqueda
        query.exec((err, articles) => {
            if(err) {
                return res.status(500).send({
                    status: 'error',
                    message: 'Error al devolver los articulos !!!'
                })
            }
            if(!articles) {
                return res.status(404).send({
                    status: 'error',
                    message: 'No existen articulos para mostrar !!!'
                })
            }
            return res.status(200).send({
                status: 'success',
                articles
            })
        })
    },

    getArticle: (req, res) => {
        // Recibir el id de la URL
        var articleId = req.params.id
        // Comprobar que existe id
        if(!articleId || articleId == null) {
            return res.status(404).send({
                status: 'error',
                message: 'No existe el articulo !!!'
            })
        }
        // Buscar articulo
        Article.findById(articleId, (err, article) => {
            if(err){
                return res.status(500).send({
                    status: 'error',
                    message: 'Error al devolver los datos'
                })
            }
            if(!article) {
                return res.status(404).send({
                    status: 'error',
                    message: 'No existe el articulo'
                })
            }
            return res.status(200).send({
                status:'success',
                article
    
            })
        })
    },

    update: (req, res) => {
        var articleId = req.params.id

        var params = req.body

        try {
            var validate_title = !validator.isEmpty(params.title)
            var validate_content = !validator.isEmpty(params.content)
        }catch(error) {
            return res.status(200).send({
                status: 'error',
                message: 'Faltan datos por enviar !!!'
            })
        }
        if(validate_title && validate_content) {
            Article.findOneAndUpdate({_id: articleId}, params, {new: true}, (err, articleUpdated) => {
                if(err) {
                    return res.status(500).send({
                        status: 'error',
                        message: 'Error al actualizar'
                    })
                }
                if(!articleUpdated) {
                    return res.status(404).send({
                        status: 'error',
                        message: 'No existe el articulo'
                    })
                }

                return res.status(200).send({
                    status: 'success',
                    article: articleUpdated
                })
            })
        }
        else {
            return res.status(200).send({
                status: 'error',
                message: 'La validacion no es correcta !!!'
            })
        }
    },

    delete: (req, res) => {
        var articleId = req.params.id

        // Find
        Article.findOneAndRemove({_id: articleId}, (err, articleRemoved) => {
            if(err) {
                return res.status(500).send({
                    status: 'error',
                    message: 'Error al borrar'
                })
            }
            if(!articleRemoved) {
                return res.status(404).send({
                    status: 'error',
                    message: 'No se ha borrado el articulo, posiblemenente no exista !!!'
                })
            }
            return res.status(200).send({
                status: 'success',
                article: articleRemoved
            })
        })
    },

    upload: (req, res) => {
        // Configurar el modulo connect multiparty router/article.js

        // Recoger el fichero de la peticion
        var file = 'Imagen no subida'
        if(!req.files) {
            return res.status(404).send({
                status: 'error',
                message: file
            })
        }
        // Conseguir el nombre y la extension del archivo
        var file = req.files.file0.path
        var file_split = file.split('\\')

        var file_name = file_split[2]
        var extension = file_name.split('\.')
        var file_ext = extension[1]
        // LINUX O MAC 
        // var file_split = file.split('/')
        // Comprobar la extension
        if(file_ext != 'png' && file_ext != 'jpg' && file_ext != 'jpeg' && file_ext != 'gif') {
            // Borrar archivos
            fs.unlink(file, (err) => {
                return res.status(200).send({
                    status: 'error',
                    message: 'La extension de la imagen no es valida'
                })
            })
        }
        else {
            var articleId = req.params.id
            // Buscar el articulo, asignarle nombre y actualizarlo
            Article.findOneAndUpdate({_id: articleId}, {image: file_name}, {new:true}, (err, articleUpdated) => {
                if(err || !articleUpdated) {
                    return res.status(200).send({
                        status: 'error',
                        message: 'Error al guardar la imagen del articulo'
                    })
                }
                return res.status(200).send({
                    status: 'success',
                    article: articleUpdated
                })
            })

        }
    },

    getImage: (req, res) => {
        var file = req.params.image
        var file_path = './upload/articles/'+file

        fs.exists(file_path, (exists) => {
            if(exists) {
                console.log(exists)
                return res.sendFile(path.resolve(file_path))
            }
            else {
                return res.status(404).send({
                    status: 'error',
                    message: 'La imagen no existe'
                })
            }
        })
        return res.status(200).send({
            status: 'success',
            file
        })
    },

    search: (req, res) => {
        // Obtener el string a buscar
        var searchString = req.params.search
        // Buscar con un OR
        Article.find({
            "$or": [
                // utilizando regex busca en la variable y en opciones en caso de estar incluido se utiliza is
                { 'title': {'$regex': searchString, '$options': 'i'}},
                { 'content': {'$regex': searchString, '$options': 'i'}}
            ]
        })
        .sort([['date', 'descending']])
        .exec((err, articles) => {
            if(err) {
                return res.status(500).send({
                    status: 'error',
                    message: 'Error en la petición'
                })
            }
            if(!articles || articles.length <= 0) {
                return res.status(404).send({
                    status: 'error',
                    message: 'No existen articulos que coincidan con tu busqueda !!!'
                })
            }
            return res.status(200).send({
                status: 'success',
                articles
            })
        })
    }
}

module.exports = controller