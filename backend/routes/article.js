'use strict'

var express = require('express')
const { route } = require('../app')
var ArticleController = require('../controllers/article')
var router = express.Router()
var multipart = require('connect-multiparty')
var md_upload = multipart({uploadDir: './upload/articles'})
//
// Lista de rutas
router.post('/datos_curso', ArticleController.datosCurso)
router.get('/prueba', ArticleController.test)
router.post('/save', ArticleController.save)
router.get('/articles/:last?', ArticleController.getArticles)
//router.get('/articles', ArticleController.getArticles)
router.get('/article/:id', ArticleController.getArticle)
router.put('/article/:id', ArticleController.update)
router.delete('/article/:id', ArticleController.delete)
router.post('/upload_image/:id', md_upload, ArticleController.upload)
router.get('/get_image/:image', ArticleController.getImage)
router.get('/search/:search', ArticleController.search)

module.exports = router