'use strict'

var mongoose = require('mongoose')
var app = require('./app')
var port = 3900

mongoose.Promise = global.Promise
mongoose.connect('mongodb://localhost:27017/api_rest_blog', { useNewUrlParser: true }, {useFindAndModify: false }).then(() => {
    console.log('La conexión a la  base de datos ha sido exitosa...')

    // Crear servidor y ponerlo a escuchar peticiones HTTP
    app.listen(port, () => {
        console.log('Servior corriendo en http://localhost:'+port)
    })
})